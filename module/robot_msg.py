from django.conf import settings
from linebot import LineBotApi
from linebot.models import TextSendMessage, ImageSendMessage, StickerSendMessage, LocationSendMessage, QuickReply, QuickReplyButton, MessageAction

line_bot_api = LineBotApi(settings.LINE_CANNEL_ACCESS_TOKEN)
ErrorMsg =TextSendMessage(text = 'Django 伺服器發生錯誤')

def sendText(event):
    try:
        message = TextSendMessage(
            text = "哪來的回哪去，不要在這邊吵鬧，這裡已經夠吵了!"
        )

        line_bot_api.reply_message(event.reply_token, message)
    except:
        line_bot_api.reply_message(event.reply_token, ErrorMsg)

def TestInput(event):
    print(event.message.text)

def sentResText(event, res_text):
    line_bot_api.reply_message(event.reply_token, TextSendMessage(text = res_text))

def sendImage(event):
    try:
        message = ImageSendMessage(
            original_content_url= 'https://cdn2.ettoday.net/images/2725/d2725549.jpg',
            preview_image_url= 'https://cdn2.ettoday.net/images/2725/d2725549.jpg',
        )
        line_bot_api.reply_message(event.reply_token, message)
    except:
        line_bot_api.reply_message(event.reply_token, ErrorMsg)

def sendLocation(event):
    try:
        message = LocationSendMessage(
            title = '中興大學',
            address = '台中市南區興大路145號',
            latitude = 24.123751,
            longitude = 120.675018,
        )
        line_bot_api.reply_message(event.reply_token, message)
    except:
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text="發生錯誤！"))

def sendStick(event):
    try:
        message = StickerSendMessage(
            package_id = '1',
            sticker_id = '2'
        )
        line_bot_api.reply_message(event.reply_token, message)
    except:
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text="發生錯誤！"))

def sendMulti(event):
    try:
        message = [
            TextSendMessage(
                text = "您好，我是 Line BoT!"
            ),
            TextSendMessage(
                text = "這是我的自拍照"
            ),
            ImageSendMessage(
                original_content_url = "https://img.freepik.com/premium-vector/bot-chat-say-hi-robots-that-are-programmed-talk-customers-online_68708-622.jpg",
                preview_image_url = "https://img.freepik.com/premium-vector/bot-chat-say-hi-robots-that-are-programmed-talk-customers-online_68708-622.jpg"
            ),
            TextSendMessage(
                text = "我現在住在中興大學位置是："
            ),
            LocationSendMessage(
                title = '中興大學',
                address = '台中市南區興大路145號',
                latitude = 24.123751,
                longitude = 120.675018,
            ),
        ]
        
        line_bot_api.reply_message(event.reply_token, message)

    except:
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text="發生錯誤！"))
        quest_text = event.message.text
        res_text = ''
        bot_res.TestInput(event)
        if quest_text == '@夕陽':
            res_text = "你活該!"
            bot_res.sentResText(event, res_text)
        elif quest_text == "@都市":
            bot_res.sendText(event)
        elif quest_text == "@瀑布":
            bot_res.sendImage(event)
        elif quest_text == "@山":
            bot_res.sendLocation(event)
        elif quest_text == "@湖泊":
            bot_res.sendStick(event)
        elif quest_text == "@指引":
            bot_res.sendMulti(event)
        else:
            res_text = "我不知道你在說甚麼。"
            bot_res.sentResText(event, res_text)
