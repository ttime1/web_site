"""mblog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
# from mainsite.views import homepage, showpost, homepage_comm_form
from mainsite.views import *
from webhook_bot.views import *


urlpatterns = [
    path('admin/', admin.site.urls),
    path('',homepage_comm_form),
    path('templates/index.html',homepage),    
    path('post/<slug:slug>', showpost),
    path('templates/comm_form.html', homepage_comm_form),
    path('templates/comm_A.html', homepage_comm_A),
    path('templates/comm_B.html', homepage_comm_B),
    path('templates/comm_C.html', homepage_comm_C),
    path('templates/comm_partner.html', homepage_comm_partner),
    path('templates/comm_S.html', homepage_comm_S),
    path('callback', callback),
    path('templates/upload_ui.html', image_upload_method),
    
]
