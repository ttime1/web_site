from django import forms
from .models import upload_img

class ImageFileUploadForm(forms.ModelForm):
    class Meta:
        model = upload_img
        fields = ('img_title', 'img_path')