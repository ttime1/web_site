from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Post
from datetime import datetime
from django.http import JsonResponse
from .forms import ImageFileUploadForm
# Create your views here.
def homepage(request):
    posts = Post.objects.all()
    post_lists = list()
    now = datetime.now()
    return render(request, 'index.html', locals())
    # for count, post in enumerate(posts):
    #     post_lists.append("No.{}:".format(str(count)) + str(post)+"<br>") 
    #     post_lists.append("<small>"+str(post.body)+"</small><br><br>")
    # return HttpResponse(post_lists)

def showpost(request, slug):
    try:
        post = Post.objects.get(slug = slug)
        if post != None:
            return render(request, 'post.html', locals())
    except:
        return redirect('/')

def homepage_comm_form(request):
    posts = Post.objects.all()
    now = datetime.now()
 
    return render(request, 'comm_form.html', locals())
    
def homepage_comm_A(request):
    posts = Post.objects.all()
    now = datetime.now()
 
    return render(request, 'comm_A.html', locals())


def homepage_comm_B(request):
    posts = Post.objects.all()
    now = datetime.now()
 
    return render(request, 'comm_B.html', locals())

def homepage_comm_C(request):
    posts = Post.objects.all()
    now = datetime.now()
 
    return render(request, 'comm_C.html', locals())

def homepage_comm_partner(request):
    posts = Post.objects.all()
    now = datetime.now()
 
    return render(request, 'comm_partner.html', locals())
    
def homepage_TRY(request):
    posts = Post.objects.all()
    now = datetime.now()
 
    return render(request, 'TRY.html', locals())

def homepage_comm_S(request):
    posts = Post.objects.all()
    now = datetime.now()
 
    return render(request, 'comm_S.html', locals())


def image_upload_method(request):
    if request.method == "POST":
        form = ImageFileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return JsonResponse({'error':False, 'message': 'Uploaded Successfully.'})
        else:
            return JsonResponse({'error':True, 'errors': form.errors})
    else:
        form = ImageFileUploadForm()
        return render(request, 'upload_ui.html', {'form': form})