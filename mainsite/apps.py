from django.apps import AppConfig


class MainsiteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mainsite'

class UploadImgConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'upload_img'
