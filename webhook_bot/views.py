from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from linebot import LineBotApi, WebhookParser
from linebot.exceptions import InvalidSignatureError, LineBotApiError
from linebot.models import MessageEvent, TextSendMessage, TextMessage

from module import robot_msg

# Create your views here.

line_bot_api = LineBotApi(settings.LINE_CANNEL_ACCESS_TOKEN)
parser = WebhookParser(settings.LINE_CHANNEL_SECRET)

@csrf_exempt
def callback(request):
    if request.method == 'POST':
        signature = request.META['HTTP_X_LINE_SIGNATURE']
        body = request.body.decode('utf-8')

        try:
            events = parser.parse(body, signature)
        except InvalidSignatureError:
            return HttpResponseForbidden()
        except LineBotApiError:
            return HttpResponseBadRequest()

        for event in events:
            if isinstance(event, MessageEvent):
                if isinstance(event.message, TextMessage):
                # print(event.message.text)
                # 文字處理
                    quest_text=event.message.text
                    res_text = ''
                    if quest_text =='@夕陽':
                        mText ="丞相，起風了!"
                        line_bot_api.reply_message(event.reply_token, TextSendMessage(text = mText))
                    elif quest_text =='@都市':
                        robot_msg.sendText(event)
                    elif quest_text == "@瀑布":
                        robot_msg.sendImage(event)
                    elif quest_text == "@山":
                        robot_msg.sendLocation(event)
                    elif quest_text == "@湖泊":
                        robot_msg.sendStick(event)
                    elif quest_text == "@指引":
                        robot_msg.sendMulti(event)
                    else:
                        mText ="滾，一定是你搞錯，我的字典裡沒有這件事!"

                
            # line_bot_api.reply_message(event.reply_token, TextSendMessage(text = event.message.text))
            # line_bot_api.reply_message(event.reply_token, TextSendMessage(text = mText))
        return HttpResponse()
    else:
        return HttpResponseBadRequest()