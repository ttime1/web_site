from django.apps import AppConfig


class WebhookBotConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'webhook_bot'
